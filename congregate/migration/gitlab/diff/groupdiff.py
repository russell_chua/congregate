from os.path import getmtime
from datetime import timedelta
from gitlab_ps_utils.misc_utils import get_rollback_log
from gitlab_ps_utils.dict_utils import rewrite_json_list_into_dict
from gitlab_ps_utils.json_utils import read_json_file_into_object

from congregate.migration.diff.basediff import BaseDiffClient
from congregate.migration.gitlab import constants
from congregate.migration.gitlab.api.groups import GroupsApi
from congregate.migration.gitlab.api.issues import IssuesApi
from congregate.migration.gitlab.api.merge_requests import MergeRequestsApi
from congregate.migration.gitlab.groups import GroupsClient
from congregate.helpers.migrate_utils import get_full_path_with_parent_namespace, is_top_level_group
from congregate.helpers.utils import is_dot_com


class GroupDiffClient(BaseDiffClient):
    '''
        Extension of BaseDiffClient focused on finding the differences between migrated groups
    '''

    def __init__(self, staged=False, subgroups_only=False,
                 rollback=False, processes=None):
        super().__init__()
        self.groups_api = GroupsApi()
        self.issues_api = IssuesApi()
        self.mr_api = MergeRequestsApi()
        self.groups = GroupsClient()
        self.results_path = f"{self.app_path}/data/results/group_migration_results.json"
        self.results = rewrite_json_list_into_dict(
            read_json_file_into_object(self.results_path))
        self.results_mtime = getmtime(self.results_path)
        self.rollback = rollback
        self.processes = processes
        self.keys_to_ignore = constants.GROUP_DIFF_KEYS_TO_IGNORE
        if is_dot_com(self.config.destination_host) or is_dot_com(
                self.config.source_host):
            self.keys_to_ignore.append("ldap_group_links")
        if staged:
            self.source_data = read_json_file_into_object(
                "%s/data/staged_groups.json" % self.app_path)
        else:
            self.source_data = read_json_file_into_object(
                "%s/data/groups.json" % self.app_path)
        # Filter out relevant groups
        staged_top_groups = [
            g for g in self.source_data if is_top_level_group(g)]
        staged_subgroups = [
            g for g in self.source_data if not is_top_level_group(g)]
        self.source_data = staged_subgroups if subgroups_only else staged_top_groups

    def generate_diff_report(self, start_time):
        diff_report = {}
        self.log.info(
            f"{get_rollback_log(self.rollback)}Generating Group Diff Report")
        self.log.warning(
            f"Passed since migration time: {timedelta(seconds=start_time - self.results_mtime)}")
        results = self.multi.handle_multi_process_write_to_file_and_return_results(
            self.generate_single_diff_report, self.return_only_accuracies, self.source_data, f"{self.app_path}/data/results/group_diff.json", processes=self.processes)

        for result in results:
            diff_report.update(result)

        diff_report["group_migration_results"] = self.calculate_overall_stage_accuracy(
            diff_report)

        return diff_report

    def generate_single_diff_report(self, group):
        diff_report = {}
        group_path = get_full_path_with_parent_namespace(group["full_path"])
        if self.results.get(group_path) and (self.asset_exists(self.groups_api.get_group,
                                                               self.results[group_path].get("id")) or isinstance(self.results.get(group_path), int)):
            group_diff = self.handle_endpoints(group)
            diff_report[group_path] = group_diff
            try:
                diff_report[group_path]["overall_accuracy"] = self.calculate_overall_accuracy(
                    diff_report[group_path])
                return diff_report
            except Exception as e:
                self.log.info(
                    f"Failed to generate diff for {group_path} with error:\n{e}")
        return {
            group_path: {
                "error": "group missing",
                "overall_accuracy": {
                    "accuracy": 0,
                    "result": "failure"
                }
            }
        }

    def handle_endpoints(self, group):
        group_diff = {}
        group_diff["/groups/:id"] = self.generate_group_diff(
            group, self.groups_api.get_group, critical_key="full_path")

        if not self.rollback:
            group_diff["/groups/:id/variables"] = self.generate_group_diff(
                group, self.groups_api.get_all_group_variables, obfuscate=True)
            group_diff["/groups/:id/members"] = self.generate_group_diff(
                group, self.groups_api.get_all_group_members)
            group_diff["/groups/:id/boards"] = self.generate_group_diff(
                group, self.groups_api.get_all_group_issue_boards)
            group_diff["/groups/:id/labels"] = self.generate_group_diff(
                group, self.groups_api.get_all_group_labels)
            group_diff["/groups/:id/milestones"] = self.generate_group_diff(
                group, self.groups_api.get_all_group_milestones)
            group_diff["/groups/:id/projects"] = self.generate_group_diff(
                group, self.groups_api.get_all_group_projects)
            group_diff["/groups/:id/subgroups"] = self.generate_group_diff(
                group, self.groups_api.get_all_group_subgroups)
            group_diff["/groups/:id/custom_attributes"] = self.generate_group_diff(
                group, self.groups_api.get_all_group_custom_attributes)
            group_diff["/groups/:id/registry/repositories"] = self.generate_group_diff(
                group, self.groups_api.get_all_group_registry_repositories)
            group_diff["/groups/:id/badges"] = self.generate_group_diff(
                group, self.groups_api.get_all_group_badges)
            group_diff["/groups/:id/clusters"] = self.generate_group_diff(
                group, self.groups_api.get_all_group_clusters)

            if self.config.source_tier not in ["core", "free"]:
                group_diff["/groups/:id/hooks"] = self.generate_group_diff(
                    group, self.groups_api.get_all_group_hooks)
            if self.config.source_tier not in [
                    "core", "free", "starter", "bronze"]:
                group_diff["/groups/:id/epics"] = self.generate_group_diff(
                    group, self.groups_api.get_all_group_epics)
        return group_diff

    def generate_group_diff(self, group, endpoint, **kwargs):
        return self.generate_diff(group, "full_path", endpoint,
                                  parent_group=self.config.dstn_parent_group_path, **kwargs)
